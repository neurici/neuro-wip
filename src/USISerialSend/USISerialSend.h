#ifndef INCLUDED_USISERIALSEND_H
#define INCLUDED_USISERIALSEND_H

#include <stdint.h>

enum USISERIAL_SEND_STATE { AVAILABLE, FIRST, SECOND };

void USI_setup();
void usiserial_send_byte(uint8_t data);
bool usiserial_send_available();

extern volatile USISERIAL_SEND_STATE usiserial_send_state;
static inline USISERIAL_SEND_STATE usiserial_send_get_state(void)
{
    return usiserial_send_state;
}
#endif // include guard
